﻿using NUnit.Framework;
using NSubstitute;

public class UrlRequestBuilderTests
{
	IUrlRequestBuilder _urlRequestBuilder;

	[SetUp]
	public void Setup()
	{
		_urlRequestBuilder = Substitute.For<IUrlRequestBuilder>();
	}

	[Test]
	public void GivenAnInput_FromNewSearchMade_CheckValidBuildRequestSentOnce()
	{
		var sut = _urlRequestBuilder;

		int counter = 1;
		
		sut.When(i => i.BuildRequest(0))
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenAnInput_FromCheckValidBuildRequest_BuildRequestSentOnce()
	{
		var sut = _urlRequestBuilder;

		int counter = 1;

		sut.When(i => i.BuildRequest(0))
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenAnInput_FromBuildRequest_SendRequestToHandlerSentOnce()
	{
		var sut = _urlRequestBuilder;

		int counter = 1;

		sut.When(i => i.BuildRequest(0))
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}
}