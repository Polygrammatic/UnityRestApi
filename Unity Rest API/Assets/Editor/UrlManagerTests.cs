﻿using NUnit.Framework;
using NSubstitute;

public class UrlManagerTests
{
	IUrlManager _urlManager;

	[SetUp]
	public void Setup()
	{
		_urlManager = Substitute.For<IUrlManager>();
	}

	[Test]
    public void GivenRequestToGetRemainingElements_GetRemainingElementsToSpawn_ReturnZeroInt()
	{
		var sut = _urlManager;
		sut.CountElementsLeftToSpawn();
		Assert.AreEqual(0, sut.CountElementsLeftToSpawn());
	}

	[Test]
	public void GivenRequestToGetRemainingElements_GetRemainingElementsToSpawn_CheckIsCalledOnce()
	{
		var sut = _urlManager;
		int counter = 1;
		
		sut.When(i => i.CountElementsLeftToSpawn())
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenRequestToResetElementCounter_MakeSureZeroReturned()
	{
		var sut = _urlManager;
		sut.ResetElementCreatedCounter();
		Assert.AreEqual(0, sut.CountElementsLeftToSpawn());
	}

	[Test]
	public void GivenRequestToResetElementCreatedCounter_CheckIsCalledOnce()
	{
		var sut = _urlManager;
		int counter = 1;

		sut.When(i => i.ResetElementCreatedCounter())
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenRequest_RequestCompleted_CheckIsCalledOnce()
	{
		var sut = _urlManager;
		int counter = 1;

		string lsToDeserializse = "text";
		int liElementsToSpawn = 0;

		sut.When(i => i.RequestCompleted(lsToDeserializse, liElementsToSpawn))
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}
}