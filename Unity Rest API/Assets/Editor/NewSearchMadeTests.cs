﻿using NUnit.Framework;
using NSubstitute;

public class NewSearchMadeTests
{
	IUrlRequestBuilder _urlRequestBuilder;
	IUrlManager _urlManager;
	IUserInterfaceManager _userInterfaceManager;
	INewSearchMade _newSearchMade;

	[SetUp]
	public void Setup()
	{
		_urlRequestBuilder = Substitute.For<IUrlRequestBuilder>();
		_urlManager = Substitute.For<IUrlManager>();
		_userInterfaceManager = Substitute.For<IUserInterfaceManager>();
		_newSearchMade = Substitute.For<INewSearchMade>();
	}

	[Test]
	public void GivenAnInputOnClick_UrlRequestBuilderIsCalled_StartsBuildRequestOnce()
	{
		var sut = _urlRequestBuilder;
		int liOffset = 0;
		int counter = 1;

		_newSearchMade.Construct(sut, _urlManager, _userInterfaceManager);

		sut.When(i => i.BuildRequest(liOffset))
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenAnInputOnClick_UrlManagerIsCalledOnce_ResetsElementsSpawnedCounter()
	{
		var sut = _urlManager;
		int counter = 1;

		_newSearchMade.Construct(_urlRequestBuilder, sut, _userInterfaceManager);

		sut.When(i => i.ResetElementCreatedCounter())
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}

	[Test]
	public void GivenAnInputOnClick_UiManagerIsCalled_ClearsExistingElementsOnce()
	{
		var sut = _userInterfaceManager;
		int counter = 1;

		_newSearchMade.Construct(_urlRequestBuilder, _urlManager, sut);

		sut.When(i => i.ClearElements())
			.Do(i => counter++);

		Assert.AreEqual(1, counter);
	}
}