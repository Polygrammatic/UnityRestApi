﻿using NUnit.Framework;
using NSubstitute;
using UnityEngine.Networking;

public class UrlRequestHandlerTests

{
	IWebRequestFactory _webRequestFactory;
	IWebRequest _webRequest;
	IUrlManager _urlManager;
	UrlRequestHandler _urlRequestHandler;

	const string lsReq = "text";

	[SetUp]
	public void Setup()
	{
		_webRequestFactory = Substitute.For<IWebRequestFactory>();
		_webRequest = Substitute.For<IWebRequest>();
		_urlManager = Substitute.For<IUrlManager>();
		_urlRequestHandler = new UrlRequestHandler();

		_webRequestFactory.Create(lsReq).Returns(_webRequest);
	}

	[Test]
	public void GivenValidRequestString_WhenWebRequestIsNetworkError_ThenErrorReturned()
	{
		string expected = "error";
		int liLimit = 10;

		_webRequest.isNetworkError.Returns(true);
		_webRequest.error.Returns(expected);

		var sut = _urlRequestHandler;


		sut.Construct(_webRequestFactory, _urlManager);
		var iterator = sut.SendRequest(lsReq, liLimit);
		while (iterator.MoveNext()) { }
		
		_urlManager.Received(1).RequestCompleted(Arg.Is<string>(s => s == expected), Arg.Is<int>(i => i == liLimit));
	}

	[Test]
	public void GivenValidRequestString_WhenWebRequestIsHttpError_ThenErrorReturned()
	{
		string expected = "error";
		int liLimit = 10;

		_webRequest.isHttpError.Returns(true);
		_webRequest.error.Returns(expected);

		var sut = _urlRequestHandler;

		sut.Construct(_webRequestFactory, _urlManager);
		var iterator = sut.SendRequest(lsReq, liLimit);
		while (iterator.MoveNext()) { }
		
		_urlManager.Received(1).RequestCompleted(Arg.Is<string>(s => s == expected), Arg.Is<int>(i => i == liLimit));
	}

	[Test]
	public void GivenValidRequestString_WhenNoError_ThenApiReturned()
	{
		string expected = "Expected";
		int liLimit = 10;

		var downloadHandler = Substitute.For<DownloadHandler>();
		downloadHandler.text.Returns(expected);
		_webRequest.downloadHandler.Returns(downloadHandler);

		var sut = _urlRequestHandler;

		sut.Construct(_webRequestFactory, _urlManager);
		var iterator = sut.SendRequest(lsReq, liLimit);
		while (iterator.MoveNext()) { }

		_urlManager.Received(1).RequestCompleted(Arg.Is<string>(s => s == expected), Arg.Is<int>(i => i == liLimit));
	}
}
