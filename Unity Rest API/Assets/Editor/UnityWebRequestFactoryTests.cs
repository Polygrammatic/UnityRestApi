﻿using NUnit.Framework;

public class UnityWebRequestFactoryTests
{
	[Test]
    public void UnityWebrequestFactoryReturnsUnityWebRequestWrapper()
	{
		var sut = new UnityWebRequestFactory();
		var actual = sut.Create("text");

		Assert.IsAssignableFrom<UnityWebRequestWrapper>(actual);
	}
}
