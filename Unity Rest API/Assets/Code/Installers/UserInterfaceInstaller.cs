using Zenject;

public class UserInterfaceInstaller : Installer<UserInterfaceInstaller>
{
	public override void InstallBindings()
    {
		Container.Bind<INewSearchMade>().To<NewSearchMade>().FromComponentSibling().AsSingle();

		Container.Bind<IUserInterfaceManager>().To<UserInterfaceManager>().FromComponentSibling().AsSingle();
		Container.Bind<UserInterfaceManager>().FromComponentSibling();
	}
}