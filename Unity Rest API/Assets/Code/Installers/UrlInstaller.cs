using Zenject;

public class UrlInstaller : Installer<UrlInstaller>
{
	public override void InstallBindings()
    {
		Container.Bind<IWebRequestFactory>().To<UnityWebRequestFactory>().AsTransient();
		Container.Bind<IWebRequest>().To<UnityWebRequestWrapper>().AsTransient();

		Container.Bind<IUrlRequestBuilder>().To<UrlRequestBuilder>().FromComponentSibling().AsTransient();
		Container.Bind<IUrlManager>().To<UrlManager>().FromComponentSibling().AsSingle();
		Container.Bind<UrlManager>().FromComponentSibling();
		Container.Bind<UrlRequestBuilder>().FromComponentSibling();
		Container.Bind<UrlDeserializeResponse>().FromComponentSibling();
		Container.Bind<UrlRequestHandler>().AsSingle();
	}
}