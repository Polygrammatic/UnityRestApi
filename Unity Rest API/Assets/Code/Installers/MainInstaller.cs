using Zenject;

public class MainInstaller : MonoInstaller
{
	public override void InstallBindings()
    {
		UrlInstaller.Install(Container);
		UserInterfaceInstaller.Install(Container);
	}
}