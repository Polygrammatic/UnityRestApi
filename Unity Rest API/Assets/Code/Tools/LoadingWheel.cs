﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingWheel : MonoBehaviour
{
	[SerializeField]
	private Sprite[] _sprites = new Sprite[9];

	private int _miCurrentImageIndex = 0;
	private float _framesPerSecond = 0.1f;
	private float _timeRemaining = 0f;

	private void Awake()
	{
		_timeRemaining = _framesPerSecond;
	}

	public void Update()
	{
		gameObject.GetComponent<Image>().sprite = _sprites[_miCurrentImageIndex];

		if (_timeRemaining > 0.0f)
		{
			_timeRemaining -= Time.deltaTime;
			if (_miCurrentImageIndex == 8) _miCurrentImageIndex = 0;
		}
		else if (_timeRemaining < 0.0f)
		{
			_miCurrentImageIndex += 1;
			_timeRemaining = _framesPerSecond;
		}
	}
}