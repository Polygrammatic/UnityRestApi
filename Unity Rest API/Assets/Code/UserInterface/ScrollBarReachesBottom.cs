﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

public class ScrollBarReachesBottom : MonoBehaviour
{
	[SerializeField]
	private GameObject _scrollBar;
	[SerializeField]
	private GameObject _loadingWheel;

	private int _miUrlOffest = 10;
	private Scrollbar _sb;
	
	private IUrlRequestBuilder _urlRequestBuilder;
	private UrlManager _urlManager;

	private BoolReactiveProperty _rbScrollBarConditionsMet = new BoolReactiveProperty();

	[Inject]
	public void Construct(IUrlRequestBuilder lUrlRequestBuilder, UrlManager lurlManager)
	{
		_urlRequestBuilder = lUrlRequestBuilder;
		_urlManager = lurlManager;
	}

	private void Awake()
	{
		if (_scrollBar.activeSelf == true) _scrollBar.SetActive(false);
		_sb = _scrollBar.GetComponent<Scrollbar>();

		_urlManager.rbRequestCompleted.Value = true;
		_rbScrollBarConditionsMet.Value = false;
	}

	private void Start()
	{
		_urlManager.rbRequestCompleted.Subscribe(
			b => { if (b) _loadingWheel.SetActive(false); else if (!b) _loadingWheel.SetActive(true); });

		_rbScrollBarConditionsMet.Subscribe(
			b => { if (b && _urlManager.rbRequestCompleted.Value == true) NextUrlRequest(); });
	}

	private void ScrollBarConditionsMet()
	{
		if (_scrollBar.activeSelf == true && _sb.value != 1.0f && _sb.value < _sb.size / 1.5) _rbScrollBarConditionsMet.Value = true;
		else _rbScrollBarConditionsMet.Value = false;
	}

	private void FixedUpdate()
	{
		ScrollBarConditionsMet();
	}

	private void NextUrlRequest()
	{
		if (_urlManager.CountElementsLeftToSpawn() != 0)
		{
			_urlRequestBuilder.BuildRequest(_miUrlOffest);
			_miUrlOffest += 10;
			_rbScrollBarConditionsMet.Value = false;
		}
	}
}