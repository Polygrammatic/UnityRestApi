﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterfaceManager : MonoBehaviour, IUserInterfaceManager
{
	public GameObject Parent, Child, SpawnLocation, NoResultsText;

	private GameObject _mgoElementParentLocation;

	private void Awake()
	{
		_mgoElementParentLocation = GameObject.Find("/Canvas/Results/ContainerFixed/ContainerReactive/");
	}
	
	public void CreateElements(int liSpawnedElementCount, ProgramInformation lElement)
	{
		// create parent and child element
		InstantiateElementPair(liSpawnedElementCount, lElement);

		// find created elements
		GameObject lgoFoundNewParent = GameObject.Find(string.Format("/Canvas/Results/ContainerFixed/ContainerReactive/Parent_{0}:{1}", liSpawnedElementCount, lElement.Id ));
		GameObject lgoFoundNewChild = GameObject.Find(string.Format("/Canvas/Results/ContainerFixed/ContainerReactive/Child_{0}:{1}", liSpawnedElementCount, lElement.Id));

		Text lParentText = lgoFoundNewParent.gameObject.GetComponentInChildren<Text>();
		Text lChildText = lgoFoundNewChild.gameObject.GetComponentInChildren<Text>();

		// change the text, name and details of parent
		ProcessParentElement(lParentText, lElement);
		ProcessChildElement(lChildText, lElement);
		
		// Get parent button
		Button _foundNewParentButton = lgoFoundNewParent.GetComponent<Button>();

		var dictionary = new Dictionary< string, bool >();

		dictionary[lgoFoundNewParent.ToString()] = true;
		
		// add button functionality
		_foundNewParentButton.onClick.AddListener(delegate 
		{
			if (dictionary[lgoFoundNewParent.ToString()])
			{
				TitleElementClicked(lgoFoundNewParent, lgoFoundNewChild, true);
				dictionary[lgoFoundNewParent.ToString()] = false;
			}

			else if (!dictionary[lgoFoundNewParent.ToString()])
			{
				TitleElementClicked(lgoFoundNewParent, lgoFoundNewChild, false);
				dictionary[lgoFoundNewParent.ToString()] = true;
			}
		});
	}

	public void NoSearchResults(bool lbShow)
	{
		NoResultsText.SetActive(lbShow);
	}

	public int CountInstantiatedElements()
	{
		int liCount = 0;
		foreach (Transform child in _mgoElementParentLocation.transform) liCount++;
		return liCount;
	}

	public void ClearElements()
	{
		foreach (Transform child in _mgoElementParentLocation.transform) Destroy(child.gameObject);
	}

	private void InstantiateElementPair(int lLoopPosition, ProgramInformation lElement)
	{
		GameObject lParent = Instantiate(Parent);
		lParent.name = string.Format("Parent_{0}:{1}", lLoopPosition, lElement.Id);	// rename parent element
		lParent.transform.SetParent(SpawnLocation.transform, false);				// put in container

		GameObject lChild = Instantiate(Child);
		lChild.name = string.Format("Child_{0}:{1}", lLoopPosition, lElement.Id);	// rename child element
		lChild.transform.SetParent(SpawnLocation.transform, false);					// put in container
		lChild.SetActive(false);													// start children hidden
	}

	private void ProcessParentElement(Text lParentText, ProgramInformation lElement)
	{
		lParentText.color = new Color(0.1f, 0.1f, 0.1f, 1.0f);
		lParentText.transform.name = lElement.FiTitle;
		lParentText.text = lElement.FiTitle;

		string lsNoFiTitle = "Result has no Finnish title. Haulla ei löytynyt suomen kielistä otsikko.";

		if (lParentText.text == "" || lParentText.text == " ")
		{
			if (lElement.SvTitle != "" || lElement.SvTitle != " ") lParentText.text = string.Format("{0} \nSwedish: {1}", lsNoFiTitle, lElement.SvTitle);
			else if (lElement.SvTitle == "" || lElement.SvTitle == " ") lParentText.text = lsNoFiTitle;

			lParentText.fontStyle = FontStyle.Italic;
			lParentText.color = new Color(0.2f, 0.2f, 0.2f, 1.0f);
		}
	}

	private void ProcessChildElement(Text lChildText, ProgramInformation lElement)
	{
		lChildText.text = string.Format("Id: {0}\nType: {1}\nTypeMedia: {2}\nVideoType: {3}\nCollection: {4}",
			lElement.Id, lElement.Type, lElement.TypeMedia, lElement.VideoType, lElement.Collection);
	}

	void TitleElementClicked(GameObject lParent, GameObject lChild, bool lbIsActive)
	{
		if (lbIsActive)
		{
			lChild.SetActive(true);
			lbIsActive = false;
		}
		else if (!lbIsActive)
		{
			lChild.SetActive(false);
			lbIsActive = true;
		}
	}
}

public interface IUserInterfaceManager
{
	int CountInstantiatedElements();
	void ClearElements();
	void NoSearchResults(bool lbShow);
}