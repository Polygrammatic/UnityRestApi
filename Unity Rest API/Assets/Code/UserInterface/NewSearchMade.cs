﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class NewSearchMade : MonoBehaviour, INewSearchMade

{
	public Button searchButton;
	private int _offset = 0;

	private IUrlRequestBuilder _urlRequestBuilder;
	private IUrlManager _urlManager;
	private IUserInterfaceManager _userInterfaceManager;

	[Inject]
	public void Construct(IUrlRequestBuilder lUrlRequestBuilder, IUrlManager lUrlManager, IUserInterfaceManager luiManager)
	{
		_urlRequestBuilder = lUrlRequestBuilder;
		_urlManager = lUrlManager;
		_userInterfaceManager = luiManager;
	}

	private void Start()
	{
		searchButton.onClick.AddListener(delegate 
		{
			_urlRequestBuilder.CheckValidBuildRequest(_offset);
			_urlManager.ResetElementCreatedCounter();
			_userInterfaceManager.ClearElements();
		});
	}
}

public interface INewSearchMade
{
	void Construct(IUrlRequestBuilder lUrlRequestBuilder, IUrlManager lUrlManager, IUserInterfaceManager luiManager);
}