﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UrlRequestBuilder : MonoBehaviour, IUrlRequestBuilder
{
	public InputField searchQuery;
	public GameObject searchQueryNotFound;

	[HideInInspector]
	public int miLimit = 10, miOffset = 0, miElementsToSpawn = 10;

	private string[] _urlComponents = {"external.api.yle.fi", "v1/programs/items.json", "163a1437", "0125bc2a877935b12232639dc0f05a3a"};
	private IUserInterfaceManager _userInterfaceManager;
	private UrlRequestHandler _urlRequestHandler;
	private UrlManager _urlManager;

	[Inject]
	public void Construct(IUserInterfaceManager lUserInterfaceManager, UrlRequestHandler lUrlRequestHandler, UrlManager lurlManager)
	{
		_userInterfaceManager = lUserInterfaceManager;
		_urlRequestHandler = lUrlRequestHandler;
		_urlManager = lurlManager;
	}

	public void CheckValidBuildRequest(int lOffset)
	{
		_userInterfaceManager.NoSearchResults(false); // clear other notifications

		if (searchQuery.text.Replace(" ", "") != string.Empty)
		{
			searchQueryNotFound.SetActive(false);
			BuildRequest(lOffset);
		}
		else searchQueryNotFound.SetActive(true);
	}

	public void BuildRequest(int lOffset)
	{
		string lsQuery = searchQuery.text;
		string lsUrlRaw = string.Format("https://{0}/{1}?app_id={2}&app_key={3}&q={4}&limit={5}&offset={6}",
			_urlComponents[0], _urlComponents[1], _urlComponents[2], _urlComponents[3], lsQuery, miLimit, lOffset);
		string lsUrlEncoded = lsUrlRaw.Replace(" ", "%20"); // remove spaces

		SendRequestToHandler(lsUrlEncoded, miElementsToSpawn);
	}

	private void SendRequestToHandler(string lsUrlEncoded, int liElementsToSpawn)
	{
		StartCoroutine(_urlRequestHandler.SendRequest(lsUrlEncoded, miElementsToSpawn));
		_urlManager.rbRequestCompleted.Value = false;
	}
}

public interface IUrlRequestBuilder
{
	void CheckValidBuildRequest(int lOffset);
	void BuildRequest(int lOffset);
}