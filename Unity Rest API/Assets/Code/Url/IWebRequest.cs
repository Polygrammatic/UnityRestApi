﻿using System;
using UnityEngine.Networking;

public interface IWebRequest : IDisposable
{
	bool isNetworkError { get; }
	bool isHttpError { get; }
	string error { get; }
	DownloadHandler downloadHandler { get; set; }

	UnityWebRequestAsyncOperation SendWebRequest();
}

public interface IWebRequestFactory
{
	IWebRequest Create(string lString);
}

public class UnityWebRequestFactory : IWebRequestFactory
{
	public IWebRequest Create(string lString)
	{
		var unityWebRequest = UnityWebRequest.Get(lString);
		return new UnityWebRequestWrapper(unityWebRequest);
	}
}

public class UnityWebRequestWrapper : IWebRequest
{
	readonly private UnityWebRequest _unityWebRequest;

	public UnityWebRequestWrapper(UnityWebRequest lUnityWebRequest)
	{
		_unityWebRequest = lUnityWebRequest;
	}

	public DownloadHandler downloadHandler
	{
		get
		{
			return _unityWebRequest.downloadHandler;
		}
		set
		{
			_unityWebRequest.downloadHandler = value;
		}

	}

	public string error
	{
		get
		{
			return _unityWebRequest.error;
		}
	}

	public bool isHttpError
	{
		get
		{
			return _unityWebRequest.isHttpError;
		}
	}

	public bool isNetworkError
	{
		get
		{
			return _unityWebRequest.isNetworkError;
		}
	}

	public void Dispose()
	{
		_unityWebRequest.Dispose();
	}

	public UnityWebRequestAsyncOperation SendWebRequest()
	{
		return _unityWebRequest.SendWebRequest();
	}
}