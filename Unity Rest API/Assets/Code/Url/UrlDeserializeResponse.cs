﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using Zenject;

public class UrlDeserializeResponse : MonoBehaviour
{
	private UrlRequestBuilder _urlRequest;

	[Inject]
	public void Construct(UrlRequestBuilder lUrlRequest)
	{
		_urlRequest = lUrlRequest;
	}

	public IEnumerable<ProgramInformation>DeserializeResponse(string lsResults)
	{
		var parseRawJson = JSON.Parse(lsResults);
		var programInformationCollection = new List<ProgramInformation>();

		for (int i = _urlRequest.miOffset; i < _urlRequest.miOffset + _urlRequest.miLimit; i++)
		{
			programInformationCollection.Add(new ProgramInformation
			{
				MetaCount = parseRawJson["meta"]["count"],
				FiTitle = parseRawJson["data"][i]["title"]["fi"],
				SvTitle = parseRawJson["data"][i]["title"]["sv"],
				Id = parseRawJson["data"][i]["id"],
				Type = parseRawJson["data"][i]["type"],
				VideoType = parseRawJson["data"][i]["video"]["type"],
				TypeMedia = parseRawJson["data"][i]["typeMedia"],
				Collection = parseRawJson["data"][i]["collection"]
			});
		}

		return programInformationCollection;
	}
}