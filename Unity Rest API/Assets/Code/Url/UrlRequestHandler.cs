﻿using System.Collections;
using Zenject;

public class UrlRequestHandler
{
	private IWebRequestFactory _webRequestFactory;
	private IUrlManager _urlManager;
	
	[Inject]
	public void Construct(IWebRequestFactory lWebRequestFactory, IUrlManager lUrlManager)
	{
		_webRequestFactory = lWebRequestFactory;
		_urlManager = lUrlManager;
	}

	public IEnumerator SendRequest(string lsReq, int lElementsToSpawn)
	{
		// created interface for unitywebrequest for unittesting
		using (var www = _webRequestFactory.Create(lsReq)) 
		{
			yield return www.SendWebRequest();

			string lsResult =  www.isNetworkError || www.isHttpError ? www.error : www.downloadHandler.text;
			_urlManager.RequestCompleted(lsResult, lElementsToSpawn);
		}
	}
}