﻿using System.Linq;
using UnityEngine;
using Zenject;
using UniRx;

[RequireComponent(typeof(UrlRequestBuilder), typeof(UrlRequestHandler), typeof(UrlDeserializeResponse))]
[RequireComponent(typeof(NewSearchMade), typeof(ScrollBarReachesBottom), typeof(UserInterfaceManager))]

public class UrlManager : MonoBehaviour, IUrlManager
{
	[HideInInspector]
	public BoolReactiveProperty rbRequestCompleted = new BoolReactiveProperty();

	private int _miElementsLeftToSpawnCount = 0, _miCountElementsCreated = 0;

	private UrlDeserializeResponse _urlDeserialize;
	private UserInterfaceManager _uiManager;

	[Inject]
	public void Construct(UrlDeserializeResponse lUrlDeserialize, UserInterfaceManager lUiManager)
	{
		_urlDeserialize = lUrlDeserialize;
		_uiManager = lUiManager;
	}

	public int CountElementsLeftToSpawn()
	{
		return _miElementsLeftToSpawnCount;
	}

	public void ResetElementCreatedCounter()
	{
		_miCountElementsCreated = 0;
	}

	public void RequestCompleted(string lsToDeserializse, int liElementsToSpawn)
	{
		var collectionObject = _urlDeserialize.DeserializeResponse(lsToDeserializse);
		int liCountElementsLeftToSpawn = int.Parse(collectionObject.ElementAt(0).MetaCount);
		
		rbRequestCompleted.Value = true;

		// Parent + Child = 2, only want to count 1, so CountInstantiatedElements() / 2
		_miElementsLeftToSpawnCount = liCountElementsLeftToSpawn - (_uiManager.CountInstantiatedElements() / 2);

		// create elements
		if (_miElementsLeftToSpawnCount != 0)
		{
			_uiManager.NoSearchResults(false);
			for (int i = 0; i < _miElementsLeftToSpawnCount && i < 10; i++)
			{
				_uiManager.CreateElements(_miCountElementsCreated, collectionObject.ElementAt(i));
				_miCountElementsCreated++;	
			}
		}
		else if (liCountElementsLeftToSpawn == 0) _uiManager.NoSearchResults(true);
	}
}

public interface IUrlManager
{
	int CountElementsLeftToSpawn();
	void ResetElementCreatedCounter();
	void RequestCompleted(string lsToDeserializse, int liElementsToSpawn);
}