﻿public class ProgramInformation
{
	public string MetaCount { get; set; }
	public string FiTitle { get; set; }
	public string SvTitle { get; set; }

	public string Id { get; set; }
	public string Type { get; set; }
	public string VideoType { get; set; }
	public string TypeMedia { get; set; }
	public string Collection { get; set; }
}